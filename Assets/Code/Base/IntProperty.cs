using System;
using UnityEngine;

namespace Code.Base
{
    [CreateAssetMenu(fileName = "IntProperty", menuName = "GameProperties/IntProperty", order = 0)]
    public class IntProperty : ScriptableObject
    {
        [SerializeField] private int _value;
        public Action<int> OnChange;

        public void IncreaseBy(int val)
        {
            _value += val;
            OnChange?.Invoke(_value);
        }
        
        public void DecreaseBy(int val)
        {
            _value -= val;
            OnChange?.Invoke(_value);
        }

        public int Value
        {
            get => _value;
            set
            {
                _value = value;
                OnChange?.Invoke(_value);
            }
        }
        
        public void Reset()
        {
            _value = 0;
        }
    }
}