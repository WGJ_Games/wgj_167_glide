using System;
using UnityEngine;

namespace Code.Base
{
    [CreateAssetMenu(fileName = "BoolProperty", menuName = "GameProperties/BoolProperty", order = 0)]
    public class BoolProperty : ScriptableObject
    {
        [SerializeField] private bool _value;
        public Action<bool> OnChange;
        
        public bool Value
        {
            get => _value;
            set
            {
                _value = value;
                OnChange?.Invoke(_value);
            }
        }

        public void Reset()
        {
            _value = false;
        }
    }
}