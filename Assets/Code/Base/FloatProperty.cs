using System;
using UnityEngine;

namespace Code.Base
{
    [CreateAssetMenu(fileName = "FloatProperty", menuName = "GameProperties/FloatProperty", order = 0)]
    public class FloatProperty : ScriptableObject
    {
        [SerializeField] private float _value;
        public Action<float> OnChange;

        public void IncreaseBy(float val)
        {
            _value += val;
            OnChange?.Invoke(_value);
        }
        
        public void DecreaseBy(float val)
        {
            _value -= val;
            OnChange?.Invoke(_value);
        }

        public float Value
        {
            get => _value;
            set
            {
                _value = value;
                OnChange?.Invoke(_value);
            }
        }

        public void Reset()
        {
            _value = 0;
        }
    }
}