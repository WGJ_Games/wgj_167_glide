using System;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Code.Game.Chunks
{
    public class ChunkComponent : MonoBehaviour
    {
        [SerializeField] private Transform _ground;
        [SerializeField] private Transform _vB;
        [SerializeField] private Transform _hB;
        [SerializeField] private Transform _spike;
        [SerializeField] private GameObject _ramp;
        [SerializeField] private GameObject _bigRamp;
        private float _defaultPosY;
        [SerializeField] private GameObject _wallWide;
        [SerializeField] private GameObject _wallLong;

        private void Start()
        {
            if (transform.position != Vector3.zero)
            {
                InitializeChunkContent();
            }
        }

        private void InitializeChunkContent()
        {
            var rand = Random.value;
            if (!(rand < 0.5f)) return;
            if (rand > 0.25f)
            {
                SpawnRare();
            }
            else
            {
                SpawnWall();
            }
        }

        private void SpawnWall()
        {
            var rand = Random.value;
            if (rand > 0.9f)
            {
                _wallWide.SetActive(true);
            }
            else
            {
                if (rand < 0.1f)
                {
                    _wallLong.SetActive(true);
                }
            }
        }

        private void SpawnRare()
        {
            var rareRand = Random.value;
            if (rareRand > 0.85f)
            {
                SpawnSpike();
            }
            else
            {
                SpawnRamp();
            }
        }

        private void SpawnRamp()
        {
            var rand = Random.value;
            if (rand < 0.05f)
            {
                _bigRamp.SetActive(true);
            }
            else
            {
                if (rand < 0.5f)
                {
                    _ramp.SetActive(true);
                }
            }
        }

        private void SpawnSpike()
        {
            _spike.gameObject.SetActive(true);
        }

        public void PlayEffect()
        {
            _defaultPosY = _ground.localPosition.y;
            _ground.localPosition = new Vector3(0,-25,0);
            _ground.DOLocalMoveY(_defaultPosY, 2f).SetEase(Ease.OutExpo).SetDelay(Random.value*0.5f);
        }

        public void CleanUp()
        {
            _ground.DOKill();
        }

        private void OnDestroy()
        {
            _ground.DOKill();
        }
    }
}