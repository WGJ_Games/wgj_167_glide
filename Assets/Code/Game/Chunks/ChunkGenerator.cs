using System;
using System.Collections.Generic;
using UnityEngine;

namespace Code.Game.Chunks
{
    public class ChunkGenerator : MonoBehaviour
    {
        [SerializeField] private ChunkComponent ChunkPrefab;
        [SerializeField] private GameSettings _settings;
        [SerializeField] private Transform _player;
        private Vector2 _curChunk;
        private Dictionary<Vector2, ChunkComponent> _chunks;

        private void Start()
        {
            _chunks = new Dictionary<Vector2, ChunkComponent>();
            _curChunk = Vector2.zero;
            CheckChunk(false);
        }

        private void CheckChunk(bool animated)
        {
            if (_player == null)
            {
                return;
            }
            int curChunkPosX = Mathf.FloorToInt(_player.position.x / _settings.ChunkSize) * _settings.ChunkSize;
            int curChunkPosY = Mathf.FloorToInt(_player.position.z / _settings.ChunkSize) * _settings.ChunkSize;

            if (_curChunk.x != curChunkPosX || _curChunk.y != curChunkPosY)
            {
                _curChunk.x = curChunkPosX;
                _curChunk.y = curChunkPosY;
            }

            for (int x = curChunkPosX - _settings.ChunkSize * _settings.Visibility; x <= curChunkPosX + _settings.ChunkSize * _settings.Visibility; x += _settings.ChunkSize)
            {
                for (int y = curChunkPosY - _settings.ChunkSize * _settings.Visibility; y <= curChunkPosY + _settings.ChunkSize * _settings.Visibility; y += _settings.ChunkSize)
                {
                    Vector2 coord = new Vector2(x, y);

                    if (!_chunks.ContainsKey(coord))
                    {
                        GenerateChunk(coord, animated);
                    }
                }
            }

            List<Vector2> destroyChunks = new List<Vector2>();
            //remove chunks that are too far away
            //unload chunks
            foreach (KeyValuePair<Vector2, ChunkComponent> entry in _chunks)
            {
                Vector2 cp = entry.Key;
                if (Mathf.Abs(curChunkPosX - cp.x) > _settings.ChunkSize * (_settings.Visibility + 3) ||
                    Mathf.Abs(curChunkPosY - cp.y) > _settings.ChunkSize * (_settings.Visibility + 3))
                {
                    destroyChunks.Add(entry.Key);
                    entry.Value.CleanUp();
                    Destroy(entry.Value.gameObject);
                }
            }

            foreach (var chunk in destroyChunks)
            {
                _chunks.Remove(chunk);
            }
        }

        private void GenerateChunk(Vector2 coord, bool animated)
        {
            var c = Instantiate(ChunkPrefab, transform);
            c.transform.position = new Vector3(coord.x, 0, coord.y);
            if (animated)
            {
                c.PlayEffect();
            }
            _chunks.Add(coord,c);
        }


        private void Update()
        {
            CheckChunk(true);
        }
    }
}