using System;
using Code.Base;
using UnityEngine;

namespace Code.Game.Chunks
{
    public class WallComponent : MonoBehaviour
    {
        [SerializeField] private IntProperty _score;
        [SerializeField] private GameEvent _scoreReduceEvent;
        [SerializeField] private ParticleSystem _particle;
        private Rigidbody _playerRidgit;

        public ParticleDirection _direction;

        private void Start()
        {
            _playerRidgit = GameObject.Find("Player").GetComponent<Rigidbody>();
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                Destroy(gameObject);
                _score.Value -= 500;
                _scoreReduceEvent.InvokeEvent();
                
                var vel = _particle.velocityOverLifetime;
                Debug.Log("set particle Speed: " + _playerRidgit.velocity.magnitude);
                vel.x = Mathf.Clamp(_playerRidgit.velocity.x *-1/5, -10, 10);
                vel.z = Mathf.Clamp(_playerRidgit.velocity.z *-1/5, -10, 10);
                //vel.speedModifierMultiplier = _playerRidgit.velocity.magnitude/8;
                _particle.Play();
                
            }
        }
    }

    public enum ParticleDirection
    {
        Wide,
        Long
    }
}