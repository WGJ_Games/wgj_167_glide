using System;
using UnityEngine;

namespace Code.Game.Chunks
{
    public class RampComponent : MonoBehaviour
    {
        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                Destroy(gameObject, 0.75f);
            }
        }
    }
}