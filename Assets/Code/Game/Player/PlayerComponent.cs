using System;
using Code.Base;
using UnityEngine;

namespace Player
{
    public class PlayerComponent : MonoBehaviour
    {
        [SerializeField] private IntProperty _score;
        [SerializeField] private GameEvent _onDestroy;

        private void Start()
        {
            _score.Value = 0;
        }

        private void Update()
        {
            if (transform.position.y > 2f)
            {
                var score = Mathf.Floor(transform.position.y);
                _score.Value += (int) score;
            }
        }

        private void OnDestroy()
        {
            _onDestroy.InvokeEvent();
        }
    }
}