﻿using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;

public class PlayerMovment : MonoBehaviour
{
    private Rigidbody _rigidbody;
    [SerializeField] private float _speed;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        
    }

    void FixedUpdate()
    {
        Vector3 currentPos = _rigidbody.position;
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        var up  = new Vector3(0.5f,0,0.5f);
        var right = new Vector3(0.5f,0,-0.5f);
        var forwardVector = up * horizontalInput;
        var leftVector = right * (verticalInput * -1);
        
        var direction = forwardVector + leftVector;
        
        Vector3 force = direction.normalized * _speed;
        
        
        
        
        if(_rigidbody.velocity.magnitude < maxSpeed)
        {
            _rigidbody.AddForce(force);
        }
    }

    
    
    public float maxSpeed = 50;//Replace with your max speed
    
    
    
}
