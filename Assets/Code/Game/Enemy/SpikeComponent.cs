using System;
using UnityEngine;

namespace UnityTemplateProjects.AI
{
    public class SpikeComponent : MonoBehaviour
    {
        private void OnCollisionEnter(Collision other)
        {
            if (other.collider.CompareTag("Player"))
            {
                Destroy(other.gameObject);
            }
        }
    }
}