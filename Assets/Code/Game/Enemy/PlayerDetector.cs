using System;
using UnityEngine;

namespace UnityTemplateProjects.AI
{
    public class PlayerDetector : MonoBehaviour
    {
        public Transform Player;
        public bool PlayerDetected { get; set; }
        public bool PlayerInRange => (Player.position - transform.position).magnitude < 3f;
        public Vector3 PlayerDirection => Player.position - transform.position;
        public Quaternion PlayerRotation => Player.transform.rotation;

        private void Awake()
        {
            Player = GameObject.Find("Player").transform;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                PlayerDetected = true;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                PlayerDetected = false;
            }
        }

        public bool PlayerInSight()
        {
            if (!PlayerDetected) return false;

            

            Vector3 dir = PlayerDirection;
            RaycastHit hit;
            if (Physics.Raycast(transform.position, dir, out hit, Mathf.Infinity ))
            {
                Debug.DrawRay(transform.position, dir * hit.distance, Color.green);
                if (hit.collider.CompareTag("Player"))
                {
                    return true;
                }
            }
                

            return true;
        }
    }
}