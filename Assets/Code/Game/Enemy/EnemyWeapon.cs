using System;
using UnityEngine;

namespace UnityTemplateProjects.AI
{
    public class EnemyWeapon : MonoBehaviour
    {
        [SerializeField] private PlayerDetector _detector;
        [SerializeField] private float _cooldown;
        [SerializeField] private float _reloadTime;

        private void Update()
        {
            if (_detector.PlayerDetected && _cooldown <= 0)
            {
                Shoot();
                _cooldown = _reloadTime;
            }

            _cooldown -= Time.deltaTime;
        }

        private void Shoot()
        {
            
        }
    }
}