using UnityEngine;

namespace Code.Game
{
    [CreateAssetMenu(fileName = "GameSettings", menuName = "Game/Settings", order = 0)]
    public class GameSettings : ScriptableObject
    {
        [SerializeField] public int Visibility;
        [SerializeField] public int ChunkSize;
    }
}