using System;
using Code.Base;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Code.UI
{
    public class RetryButton : MonoBehaviour
    {
        [SerializeField] private Button _retry;
        [SerializeField] private GameEvent OnDefeat;
        [SerializeField] private CanvasGroup _canvas;
        [SerializeField] private IntProperty _score;
        private Vector3 _defaultPos;

        private void Start()
        {
            _retry.onClick.AddListener(() =>
            {
                DOTween.KillAll();
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            });
            OnDefeat.Callback += Show;
            _defaultPos = transform.localPosition;
            Hide();
        }

        private void OnDisable()
        {
            OnDefeat.Callback -= Show;
            _canvas.DOKill();
        }
        

        private void Hide()
        {
            _canvas.transform.localPosition += Vector3.down * 64;
            _canvas.alpha = 0;
            _canvas.interactable = false;
            _canvas.blocksRaycasts = false;
        }

        private void Show()
        {
            _canvas.DOFade(1, 0.5f);
            _canvas.transform.DOLocalMove(Vector3.zero, 2f).SetEase(Ease.OutExpo);
            _canvas.interactable = true;
            _canvas.blocksRaycasts = true;
        }
    }
}