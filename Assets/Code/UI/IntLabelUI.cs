using System;
using Code.Base;
using TMPro;
using UnityEngine;

namespace Code.UI
{
    public class IntLabelUI : MonoBehaviour
    {
        [SerializeField] private IntProperty _intProperty;
        [SerializeField] private TextMeshProUGUI _label;

        private void Start()
        {
            _intProperty.OnChange += UpdateLabel;
            UpdateLabel(_intProperty.Value);
        }

        private void OnDestroy()
        {
            _intProperty.OnChange -= UpdateLabel;
        }

        private void UpdateLabel(int obj)
        {
            _label.text = obj.ToString();
        }
    }
}