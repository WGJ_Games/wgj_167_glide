using System;
using Code.Base;
using DG.Tweening;
using UnityEngine;

namespace Code.UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class ScoreReducedUI : MonoBehaviour
    {
        [SerializeField] private GameEvent _scoreReduced;
        private CanvasGroup _canvas;

        private void Start()
        {
            _canvas = GetComponent<CanvasGroup>();
            _scoreReduced.Callback += PlayEffect;
            Hide();
        }

        private void OnDestroy()
        {
            _scoreReduced.Callback -= PlayEffect;
            _canvas.DOKill();
        }

        private void PlayEffect()
        {
            _canvas.DOKill();
            _canvas.DOFade(1, 0.25f);
            _canvas.transform.DOPunchRotation(Vector3.one*10f, 0.5f).SetDelay(0.25f);
            _canvas.transform.DOScale(Vector3.one, 0.75f).SetEase(Ease.OutExpo).OnComplete(() =>
            {
                _canvas.DOFade(0, 1f).SetDelay(0.5f).OnComplete(Hide);
            });
        }

        private void Hide()
        {
            _canvas.transform.localScale = Vector3.one*3;
            _canvas.alpha = 0;
        }
    }
}